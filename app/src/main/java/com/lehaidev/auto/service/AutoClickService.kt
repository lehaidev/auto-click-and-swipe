package com.lehaidev.auto.service

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.content.Intent
import android.graphics.Path
import android.os.Build
import android.view.accessibility.AccessibilityEvent
import com.lehaidev.auto.MainActivity
import com.lehaidev.auto.bean.Event
import com.lehaidev.auto.logd

var autoClickService: AutoClickService? = null

class AutoClickService : AccessibilityService() {

    private val events = mutableListOf<Event>()

    override fun onInterrupt() {
        // NO-OP
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        // NO-OP
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        "onServiceConnected".logd()
        autoClickService = this
        startActivity(
            Intent(this, MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        )
    }

    fun click(x: Int, y: Int) {
        "click $x $y".logd()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) return

        val path = Path()
        path.moveTo(x.toFloat(), y.toFloat() + 1000)

        val builder = GestureDescription.Builder()
        val gestureDescription = builder
            .addStroke(GestureDescription.StrokeDescription(path, 10, 1000))
            .build()

        dispatchGesture(gestureDescription, null, null)
    }

    fun swipe(x: Int, y: Int) {
        "swipe $x $y".logd()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) return

        val swipeUp = true

        val displayMetrics = resources.displayMetrics
        val height = displayMetrics.heightPixels;
        val top = height * .25;
        var mid = height * .5;
        val bottom = height * .75;
        val midX = displayMetrics.widthPixels / 2;

        val swipePath = Path()
        if (swipeUp) {
            swipePath.moveTo(midX.toFloat(), bottom.toFloat())
            swipePath.lineTo(midX.toFloat(), top.toFloat())
        } else {
            swipePath.moveTo(midX.toFloat(), top.toFloat())
            swipePath.lineTo(midX.toFloat(), bottom.toFloat())
        }
        val builder = GestureDescription.Builder()
        val gestureDescription = builder
            .addStroke(GestureDescription.StrokeDescription(swipePath, 10, 1000))
            .build()

        dispatchGesture(gestureDescription, null, null)
    }

    fun run(newEvents: MutableList<Event>) {
        events.clear()
        events.addAll(newEvents)
        events.toString().logd()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) return
        val builder = GestureDescription.Builder()
        events.forEach { builder.addStroke(it.onEvent()) }
        dispatchGesture(builder.build(), null, null)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        "AutoClickService onUnbind".logd()
        autoClickService = null
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        "AutoClickService onDestroy".logd()
        autoClickService = null
        super.onDestroy()
    }
}